package com.demo.springboot.rest;

import com.demo.springboot.CsvReader;
import com.demo.springboot.domain.dto.MovieDto;
import com.demo.springboot.domain.dto.MovieListDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
public class MovieApiController {

    List<MovieDto> movies = new ArrayList<>();

    private static final Logger LOGGER = LoggerFactory.getLogger(MovieApiController.class);

    @RequestMapping(value = "/movies")
    @ResponseBody
    @CrossOrigin
    public MovieListDto getMovies() {

        LOGGER.info("--- get movies");

        // TODO: Usluga powinna realizowac nastepujace zadania:
        // TODO: Prosze odczytac zawartosc pliku movies.csv
        // TODO: Prosze zapisac wszystkie dane z pliku movies.csv do listy znajdujacej sie w klasie MovieListDto
        // TODO: Po skonczonej implementacji prosze przetestowac w przegladarce usluge pod adresem http://127.0.0.1:8080/movies
        // TODO: Nastepnie prosze otworzyc plik movies.html

        CsvReader readCsv = new CsvReader();

        if(movies.isEmpty()) {
            movies = readCsv.myRead();
        }

        return new MovieListDto(movies);
    }

    @PostMapping("/movies")
    public ResponseEntity<MovieDto> movie(@RequestBody MovieDto inputPayload) {
        MovieDto movie = new MovieDto();
        movie.setMovieId(inputPayload.getMovieId());
        movie.setImage(inputPayload.getImage());
        movie.setTitle(inputPayload.getTitle());
        movie.setYear(inputPayload.getYear());

        String id = inputPayload.getMovieId();
        boolean containsFlag = false;

        for(MovieDto movie2: movies) {
            if(movie2.getMovieId().equals(id)) {
                containsFlag = true;
            }
        }

        if(containsFlag == false) {
            movies.add(movie);
            return new ResponseEntity<MovieDto>(HttpStatus.CREATED);
        } else {
            return new ResponseEntity<MovieDto>(HttpStatus.CONFLICT);
        }


    }
}
